/**********************************************************************
        Copyright 2014 Shannon Mackey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**********************************************************************/

#ifndef DATAMODEL_H
#define DATAMODEL_H

#include <QObject>
#include <QStateMachine>
#include <QSqlDatabase>
#include <QSqlTableModel>
#include <QUndoStack>
#include <QUuid>


class Model : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString Brand READ Brand WRITE setBrand NOTIFY BrandChanged)

public:
    explicit Model(QObject *parent = 0);
    Model(QObject *parent, QString newfilename, QString brand = "brand1");
    QList<QSqlTableModel*> *TableModels;

    void saveModel();
    void closeModel();

    QString filenameOnly();
    QString fullFilename();

    int getObjectTypeIntid(QString objname);


    QString Brand() const
    {
        return m_Brand;
    }

signals:

    void BrandChanged(QString arg);

public slots:

void setBrand(QString arg)
{
    if (m_Brand != arg) {
        m_Brand = arg;
        emit BrandChanged(arg);//TODO: connect BrandChanged to aQtual to update ui
    }
}

private:
    QSqlDatabase ModelDB;
    QUndoStack *UndoStack;

    bool createNewDB();
    void setupModels();
    void generateSQLTableModels();
    void updateTableModels();

    QString m_Brand;
};

#endif // DATAMODEL_H

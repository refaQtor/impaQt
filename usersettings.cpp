/**********************************************************************
        Copyright 2014 Shannon Mackey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**********************************************************************/

#include "usersettings.h"
#include "ui_usersettings.h"

#include <QFileDialog>

UserSettings::UserSettings(QWidget *parent) :
    QDialog(parent),
    settings(new QSettings),
    mapper(new QDataWidgetMapper),
    model(new QStandardItemModel),
    ui(new Ui::UserSettings)
{
    ui->setupUi(this);
    mapWidgets();
}

void UserSettings::mapWidgets()
{
    mapper->setModel(model);
//    mapper->addMapping(mySpinBox, 0);
//    mapper->addMapping(myLineEdit, 1);
//    mapper->addMapping(myCountryChooser, 2);
    mapper->toFirst();

}

UserSettings::~UserSettings()
{
    delete ui;
}


void UserSettings::on_SelectWeatherFileBtn_clicked()
{
    ui->WeatherLocationLabel->setText(QFileDialog::getOpenFileName(this,tr("Select Weather DB File"),QDir::toNativeSeparators(QDir::homePath()),tr("SQLite DB Files")));

}

/**********************************************************************
        Copyright 2014 Shannon Mackey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**********************************************************************/

#ifndef SITU_H
#define SITU_H

#include <QFrame>
#include <QDataWidgetMapper>

#include "localdatamanager.h"

namespace Ui {
    class Situation;
}

class Situation : public QFrame
{
    Q_OBJECT

public:
    explicit Situation(QWidget *parent = 0);
    ~Situation();

    void setDataManager(LocalDataManager *ldm);

private slots:
    void updateDisplayFields();

    void on_CityText_linkActivated(const QString &link);
    void on_SelectWeatherButton_clicked();

private:
    Ui::Situation *ui;

    LocalDataManager *DataMan;
};

#endif // SITU_H

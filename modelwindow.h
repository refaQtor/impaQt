/**********************************************************************
        Copyright 2014 Shannon Mackey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**********************************************************************/

#ifndef MODELWINDOW_H
#define MODELWINDOW_H

#include <QFrame>
#include <QGraphicsScene>

#include "model.h"

namespace Ui {
    class ModelWindow;
}

class ModelWindow : public QFrame
{
    Q_OBJECT

public:
    explicit ModelWindow(QWidget *parent = 0);
    explicit ModelWindow(QString fname, int seqno, const QColor &color, QWidget *parent = 0);
    ~ModelWindow();

    bool save();
    bool saveAs();
    void closeFile();
    QString filename() { return model->fullFilename(); }
    QString filenameOnly() { return model->filenameOnly(); }
    Model* getModel() { return model; }
    void setColor(QColor color);
    QColor color() { return m_color; }

protected:
    void closeEvent(QCloseEvent *event);

signals:
    void dirty(bool);
    void seriesDataChanged(int seqno, QList<double>* oblist) const;
    void added(int seqno, QColor color) const;
    void removed(int seqno) const;

private slots:
    void setDirty(bool);
    void modelModified(QList<QRectF>);

private:
    Ui::ModelWindow *ui;
    Model *model;
    QGraphicsScene *SceneGraph;
    int m_seqnumber;
    QColor m_color;

    bool checkSave();
    void connectUItoModel();

};

#endif // MODELWINDOW_H

/**********************************************************************
        Copyright 2014 Shannon Mackey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**********************************************************************/

#include "model.h"

#include <QMessageBox>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlField>
#include <QSqlError>
#include <QFileDialog>
#include <QFile>
#include <QTime>
#include <QList>
#include <QVector3D>
#include <QStandardPaths>

#include <QDebug>


Model::Model(QObject *parent) :
    QObject(parent)
{
    Q_UNUSED(parent)
}

Model::Model(QObject *parent, QString newfilename, QString brand) :
    QObject(parent),
    TableModels(new QList<QSqlTableModel*>()),
    UndoStack(new QUndoStack(this))
{

}


void Model::saveModel()
{
    //this needs to return bool
    //TODO:    fix these
    //    ...->submitAll();
}

void Model::closeModel()
{
    saveModel();
    ModelDB.close();
}

QString Model::filenameOnly()
{
    QString fname = QFileInfo(ModelDB.databaseName()).fileName();
    fname.remove(".qsf");
    return fname;
}

QString Model::fullFilename()
{
    return ModelDB.databaseName();
}


void Model::setupModels()
{

}

void Model::generateSQLTableModels()
{
    foreach (QString tbl, ModelDB.tables(QSql::Views)) {
        QSqlTableModel *stm = new QSqlTableModel(this,ModelDB);
        stm->setTable(tbl);
        if (tbl == "project")
            stm->setEditStrategy(QSqlTableModel::OnFieldChange);
        else
            stm->setEditStrategy(QSqlTableModel::OnManualSubmit);
        stm->select();
        TableModels->append(stm);
    }
    foreach (QString tbl, ModelDB.tables(QSql::Tables)) {
        QSqlTableModel *stm = new QSqlTableModel(this,ModelDB);
        stm->setTable(tbl);
        if (tbl == "project")
            stm->setEditStrategy(QSqlTableModel::OnFieldChange);
        else
            stm->setEditStrategy(QSqlTableModel::OnManualSubmit);
        stm->select();
        TableModels->append(stm);
    }
}

void Model::updateTableModels()
{
    for (int cnt = 0; cnt < TableModels->count(); ++cnt) {
        TableModels->at(cnt)->select();
    }
}

/**********************************************************************
        Copyright 2014 Shannon Mackey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**********************************************************************/

#include "weatherproxymodel.h"

WeatherProxyModel::WeatherProxyModel(QObject *parent) :
    QSortFilterProxyModel(parent)
{
}

bool WeatherProxyModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    QModelIndex index0 = sourceModel()->index(source_row,0,source_parent);
    QModelIndex index1 = sourceModel()->index(source_row,1,source_parent);
    QModelIndex index2 = sourceModel()->index(source_row,2,source_parent);

    return (sourceModel()->data(index0).toString().contains(CityFilterString,Qt::CaseInsensitive)
            && sourceModel()->data(index1).toString().contains(RegionFilterString,Qt::CaseInsensitive)
            && sourceModel()->data(index2).toString().contains(CountryFilterString,Qt::CaseInsensitive));
}

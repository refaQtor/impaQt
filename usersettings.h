/**********************************************************************
        Copyright 2014 Shannon Mackey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**********************************************************************/

#ifndef USERSETTINGS_H
#define USERSETTINGS_H

#include <QDialog>
#include <QSettings>
#include <QDataWidgetMapper>
#include <QStandardItemModel>

namespace Ui {
    class UserSettings;
}

class UserSettings : public QDialog
{
    Q_OBJECT

public:
    explicit UserSettings(QWidget *parent = 0);
    ~UserSettings();

private slots:

    void on_SelectWeatherFileBtn_clicked();

private:
    Ui::UserSettings *ui;

    QSettings *settings;
    QDataWidgetMapper *mapper;
    QStandardItemModel * model;

    void mapWidgets();
};

#endif // USERSETTINGS_H

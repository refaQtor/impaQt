/**********************************************************************
        Copyright 2014 Shannon Mackey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**********************************************************************/

#include "situation.h"
#include "ui_situation.h"

#include <QDebug>
#include <QSqlRecord>
#include <QSqlField>

Situation::Situation(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::Situation)
{
    ui->setupUi(this);
}

Situation::~Situation()
{
    delete ui;
}

void Situation::setDataManager(LocalDataManager *ldm)
{
    DataMan = ldm;
}

void Situation::updateDisplayFields()
{
    QSqlRecord selrec = DataMan->selectWeatherLocation();
    ui->CityText->setText(selrec.field(tr("City")).value().toString());
    ui->RegionText->setText(selrec.field(tr("Region")).value().toString());
    ui->CountryText->setText(selrec.field(tr("Country")).value().toString());
    ui->LatitudeText->setText(selrec.field(tr("Latitude")).value().toString());
    ui->LongitudeText->setText(selrec.field(tr("Longitude")).value().toString());
    ui->ElevationText->setText(selrec.field(tr("Elevation")).value().toString());

}

void Situation::on_CityText_linkActivated(const QString &link)
{
    updateDisplayFields();
}

void Situation::on_SelectWeatherButton_clicked()
{
    updateDisplayFields();
}

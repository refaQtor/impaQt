/**********************************************************************
        Copyright 2014 Shannon Mackey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**********************************************************************/

#include "results.h"
#include "ui_results.h"

Results::Results(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::Results)
{
    ui->setupUi(this);
}

Results::~Results()
{
    delete ui;
}

void Results::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void Results::on_PeriodUnit_currentIndexChanged(int index)
{
        switch ( index ) {

        case 0 :
            ui->PeriodSlider->setMaximum(365);
            ui->PeriodSlider->setTickInterval(7);
            ui->PeriodSlider->setPageStep(7);
            break;
        case 1 :
            ui->PeriodSlider->setMaximum(52);
            ui->PeriodSlider->setTickInterval(4);
            ui->PeriodSlider->setPageStep(4);
            break;
        default :
            ui->PeriodSlider->setMaximum(12);
            ui->PeriodSlider->setTickInterval(1);
            ui->PeriodSlider->setPageStep(3);
        }
        ui->PeriodSlider->setValue(0);
        ui->PeriodSlider->setFocus();
}
